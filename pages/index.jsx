import { useState } from 'react';


const token = '6a0a625c0e112d5e24067a0ce2b23828a5779556fb847870b0ce4fb9c884f284f3b1c03b5715a6002cff5b3ae5d74b81b48d9bbf96b47482d245af66efeeeb58aa1d8c968e1809a80e59abf1879fe69a97040e6fb07579f948ae0f1df38e704ee829cab7d5d053fe9b90b3b746f446f0c67c971e00205df3500a4b5131d5f48c';
function Header({ title }) {
  return <h1>{title ? title : 'Default title'}</h1>;
}

function card(moto) {
  console.log(moto.attributes.photo)

  return (
    <div key={moto.id} className="card">
      <img src={moto.attributes.image.data.attributes.url} alt="Cover"></img>
      <div className='container'>
        <h4>Brand: <b>{moto.attributes.brand}</b></h4>
        <h4>Model: <b>{moto.attributes.model}</b></h4>
        <h4>Brand: <b>{moto.attributes.price}</b></h4>
      </div>
    </div>
  )
}

export default function HomePage({ resp }) {
  const motos = resp.data;

  return (
    <div>
      <Header title="Moto catalog. 🚀" />
      <div className='catalo'>
        {motos.map((moto) => (
          card(moto)
        ))}
      </div>
    </div>
  );
}



export async function getStaticProps() {

  const res = await fetch(`http://3.69.191.48:1337/api/motos?populate=*`, {
    method: 'get',
    headers: new Headers({
      'Authorization': 'Bearer ' + token
    })
  });

  const resp = await res.json();
  return {
    props: {
      resp
    },
  }
}